package user

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/userInMongo/domain"
	"gitlab.com/userInMongo/services"
	"gitlab.com/userInMongo/utils"
	"net/http"
	"strconv"
)

func GetUser(context *gin.Context) {
	userId, err := strconv.ParseUint(context.Param("user_id"), 10, 64)

	if err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "user_id must be a number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(context, apiErr)
		return
	}
	user, apiErr := services.UserService.GetUser(userId)
	if apiErr != nil {
		utils.RespondError(context, apiErr)
		return
	}
	utils.Respond(context, http.StatusOK, user)
}

func CreateUser(context *gin.Context) {
	var user domain.User
	err := context.ShouldBindJSON(&user)
	if err != nil {
		restErr := &utils.ApplicationError{
			Message:    "invalid json body",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(context, restErr)
		return
	}
	saveErr := services.UserService.CreateUser(&user)
	if saveErr != nil {
		utils.RespondError(context, saveErr)
		return
	}
	utils.Respond(context, http.StatusCreated, "new user was added successfully!")
}

func DeleteUser(context *gin.Context) {
	userId, err := strconv.ParseUint(context.Param("user_id"), 10, 64)

	if err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "user_id must be a number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(context, apiErr)
		return
	}

	err2 := services.UserService.DeleteUser(userId)
	if err2 != nil {
		utils.RespondError(context, err2)
		return
	}

	utils.Respond(context, http.StatusOK, fmt.Sprintf("user %v was removed successfully!", userId))

}

type UpdateStruct struct {
	UserId    uint64 `json:"id"`
	FieldName string `json:"field_name"`
	Data      string `json: "data"`
}

func UpdateUser(context *gin.Context) {
	var updateStruct UpdateStruct
	err := context.ShouldBindJSON(&updateStruct)
	if err != nil {
		restErr := &utils.ApplicationError{
			Message:    "invalid json body",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(context, restErr)
		return
	}

	err2 := services.UserService.UpdateUser(updateStruct.UserId, updateStruct.FieldName, updateStruct.Data)
	if err2 != nil {
		utils.RespondError(context, err2)
		return
	}
	utils.Respond(context, http.StatusOK, "user updated successfully")

}
