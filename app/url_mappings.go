package app

import (
	"gitlab.com/userInMongo/controllers/ping"
	"gitlab.com/userInMongo/controllers/user"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)
	router.POST("/users", user.CreateUser)
	router.POST("/update_user", user.UpdateUser)
	router.GET("/delete_user/:user_id", user.DeleteUser)
	router.GET("/users/:user_id", user.GetUser)
}
